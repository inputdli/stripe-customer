<?php

return [
    'api' => env('STRIPE_API', 'https://api.stripe.com/v1'),
    'pk' => env('STRIPE_PK', ''),
    'sk' => env('STRIPE_SK', ''),
    ];
