<?php

namespace Tests\Unit;

use App\Services\Stripe\Customers\CustomerResponse;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class CustomerResponseTest extends TestCase
{
    public function testHas()
    {
        $arr = [
           'key' => 'value'
        ];

        $cr = new CustomerResponse($arr);

        self::assertTrue($cr->has('key'));
        self::assertFalse($cr->has(Str::random('5')));
    }

    public function testGet()
    {
        $value = Str::random(5);
        $arr = [
            'key' => $value
        ];

        $cr = new CustomerResponse($arr);

        self::assertEquals($value, $cr->get('key'));
        self::assertNull($cr->get($value));
    }
}
