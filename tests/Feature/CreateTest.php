<?php

namespace Tests\Feature;

use App\Services\Stripe\Customers\Create;
use App\Services\Stripe\Customers\CustomerResponse;
use App\Services\Stripe\Customers\Update;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CreateTest extends TestCase
{
    public function testCall()
    {
        $request = Http::withToken(Config::get("stripe.sk"));
        $request->baseUrl(Config::get("stripe.api"));

        $d = new Create($request);
        $r = $d->call("", ['description' => $desc=$this->faker->sentence]);
        self::assertInstanceOf(CustomerResponse::class, $r);
        self::assertEquals($r->get("description"), $desc);
    }
}
