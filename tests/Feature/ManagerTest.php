<?php

namespace Tests\Feature;

use App\Services\Stripe\Customers\All;
use App\Services\Stripe\Customers\Create;
use App\Services\Stripe\Customers\Retrieve;
use App\Services\Stripe\Customers\Update;
use App\Services\Stripe\Manager;
use Tests\TestCase;

class ManagerTest extends TestCase
{
    public function testDriver()
    {
        $m = new Manager(app());
        self::assertInstanceOf(All::class, $m->driver('all'));
        self::assertInstanceOf(Update::class, $m->driver('update'));
        self::assertInstanceOf(Retrieve::class, $m->driver('retrieve'));
        self::assertInstanceOf(Create::class, $m->driver('create'));
    }
}
