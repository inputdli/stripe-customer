<?php

namespace Tests\Feature;

use Illuminate\Console\Command;
use Tests\TestCase;

class CommandTest extends TestCase
{

    public function testCreate()
    {
        $name = $this->faker->md5;
        $this->artisan('customer:create --name='.$name)
            ->assertSuccessful();
    }

    public function testUpdate()
    {
        $name = $this->faker->md5;
        $this->artisan('customer:update cus_LBayfXeWENqjN6 --name='.$name)
            ->assertSuccessful();
    }

    public function testRetrieve()
    {
        $this->artisan('customer:retrieve cus_LBayfXeWENqjN6')
            ->assertSuccessful();
    }



    public function testList()
    {
        $this->artisan('customer:list')
            ->assertSuccessful();
    }

}
