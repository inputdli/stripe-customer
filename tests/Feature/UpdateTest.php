<?php

namespace Tests\Feature;

use App\Services\Stripe\Customers\CustomerResponse;
use App\Services\Stripe\Customers\Update;
use App\Services\Stripe\Exceptions\EmptyDataException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class UpdateTest extends TestCase
{

    public function testCall()
    {
        $request = Http::withToken(Config::get("stripe.sk"));
        $request->baseUrl(Config::get("stripe.api"));

        $d = new Update($request);
        $r = $d->call("cus_LBayfXeWENqjN6", ['description' => $desc=$this->faker->sentence]);
        self::assertInstanceOf(CustomerResponse::class, $r);
        self::assertEquals($r->get("id"), "cus_LBayfXeWENqjN6");
        self::assertEquals($r->get("description"), $desc);
    }


    public function testCallEmptyData()
    {
        $request = Http::withToken(Config::get("stripe.sk"));
        $request->baseUrl(Config::get("stripe.api"));

        $d = new Update($request);

        $this->expectException(EmptyDataException::class);

        $r = $d->call("cus_LBayfXeWENqjN6", []);
    }
}
