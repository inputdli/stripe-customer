<?php
namespace Tests\Feature;

use App\Services\Stripe\Customers\All;
use App\Services\Stripe\Customers\CustomerResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class AllTest extends TestCase
{
    public function testCall()
    {
        $request = Http::withToken(Config::get("stripe.sk"));
        $request->baseUrl(Config::get("stripe.api"));

        $d = new All($request);

        $r = $d->call();

        self::assertInstanceOf(CustomerResponse::class, $r);
        self::assertNotEmpty($r->get("data"));
    }
}
