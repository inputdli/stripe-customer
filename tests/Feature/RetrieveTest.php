<?php

namespace Tests\Feature;

use App\Services\Stripe\Customers\All;
use App\Services\Stripe\Customers\CustomerResponse;
use App\Services\Stripe\Customers\Retrieve;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class RetrieveTest extends TestCase
{

    public function testCall()
    {
        $request = Http::withToken(Config::get("stripe.sk"));
        $request->baseUrl(Config::get("stripe.api"));

        $d = new Retrieve($request);

        $r = $d->call("cus_LBayfXeWENqjN6");

        self::assertInstanceOf(CustomerResponse::class, $r);
        self::assertEquals($r->get("id"), "cus_LBayfXeWENqjN6");
    }
}
