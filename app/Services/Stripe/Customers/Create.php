<?php

namespace App\Services\Stripe\Customers;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Contracts\ResponseData;
use App\Services\Stripe\Exceptions\EmptyDataException;

class Create extends BaseDriver implements Driver
{
    /**
     * @param string $id
     * @param array $data
     * @return ResponseData
     * @throws EmptyDataException
     */
    public function call(string $id = "", array $data = []): ResponseData
    {
        if (empty($data)) {
            throw new EmptyDataException("Can not create customer by empty data.");
        }
        $response = $this->request->contentType("application/x-www-form-urlencoded")
            ->bodyFormat('form_params')
            ->post($this->getUrl(), $data);
        return new CustomerResponse($response->json());
    }

    public function getUrl(string $id = ""): string
    {
        return "customers";
    }
}
