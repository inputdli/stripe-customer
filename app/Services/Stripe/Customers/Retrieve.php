<?php

namespace App\Services\Stripe\Customers;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Contracts\ResponseData;

class Retrieve extends BaseDriver implements Driver
{

    public function call(string $id = "", array $data = []): ResponseData
    {
        $response = $this->request->get($this->getUrl($id));
        return new CustomerResponse($response->json());
    }

    public function getUrl(string $id=""): string
    {
        return sprintf("%s/%s", "customers", $id);
    }
}
