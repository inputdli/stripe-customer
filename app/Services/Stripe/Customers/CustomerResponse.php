<?php

namespace App\Services\Stripe\Customers;

use App\Services\Stripe\Contracts\ResponseData;

class CustomerResponse implements ResponseData
{
    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function has(string $name): bool
    {
        return array_key_exists($name, $this->data);
    }

    public function get(string $name)
    {
        if ($this->has($name)) {
            return $this->data[$name];
        }
        return null;
    }

    public function all()
    {
        return $this->data;
    }
}
