<?php

namespace App\Services\Stripe\Customers;

use App\Services\Stripe\Contracts\Driver as DriverInterface;
use App\Services\Stripe\Contracts\ResponseData;

class All extends BaseDriver implements DriverInterface
{
    public function call(string $id="", array $data=[]): ResponseData
    {
        $response = $this->request->get($this->getUrl());
        return new CustomerResponse($response->json());
    }

    public function getUrl($id=""): string
    {
        return "customers";
    }
}
