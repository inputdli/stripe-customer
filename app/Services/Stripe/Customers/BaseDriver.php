<?php

namespace App\Services\Stripe\Customers;

use Illuminate\Http\Client\PendingRequest;

abstract class BaseDriver
{
    protected PendingRequest $request;

    public function __construct(PendingRequest $request)
    {
        $this->request = $request;
    }
}
