<?php

namespace App\Services\Stripe;

use App\Services\Stripe\Customers\All;
use App\Services\Stripe\Customers\Create;
use App\Services\Stripe\Customers\Retrieve;
use App\Services\Stripe\Customers\Update;
use App\Services\Stripe\Exceptions\DriverException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\App;

class Manager extends \Illuminate\Support\Manager
{

    public function createAllDriver()
    {
        return new All(App::make(PendingRequest::class));
    }

    public function createCreateDriver()
    {
        return new Create(App::make(PendingRequest::class));
    }

    public function createUpdateDriver()
    {
        return new Update(App::make(PendingRequest::class));
    }

    public function createRetrieveDriver()
    {
        return new Retrieve(App::make(PendingRequest::class));
    }

    public function getDefaultDriver()
    {
        throw new DriverException("There is no default driver.");
    }
}
