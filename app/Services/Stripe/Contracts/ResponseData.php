<?php

namespace App\Services\Stripe\Contracts;

interface ResponseData
{
    public function has(string $name): bool;

    public function get(string $name);
}
