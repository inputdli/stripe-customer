<?php

namespace App\Services\Stripe\Contracts;


interface Driver
{
    public function call(string $id="", array $data=[]): ResponseData;

    public function getUrl(string $id=""): string;
}
