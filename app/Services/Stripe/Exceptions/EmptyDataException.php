<?php

namespace App\Services\Stripe\Exceptions;

use Illuminate\Http\Client\HttpClientException;

class EmptyDataException extends HttpClientException
{

}
