<?php

namespace App\Services\Stripe;

use Illuminate\Console\Command;

abstract class StripeCommand extends Command
{
    protected Manager $manager;

    public function setManager(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function outputError($error)
    {
        $this->error($error['message']);
    }
}
