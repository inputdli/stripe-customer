<?php

namespace App\Providers;

use App\Services\Stripe\Manager;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PendingRequest::class, function () {
            $request = Http::withToken(Config::get("stripe.sk"));
            $request->baseUrl(Config::get("stripe.api"));
            return $request;
        });

        $this->app->bind(Manager::class, function () {
            return new Manager($this->app);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
