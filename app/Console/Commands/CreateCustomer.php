<?php

namespace App\Console\Commands;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Manager;
use App\Services\Stripe\StripeCommand;

class CreateCustomer extends UpdateCustomer
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:create {--name=*} {--email=*} {--description=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create stripe customer';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            /** @var Driver $driver */
            $driver = $this->manager->driver('create');
            $customerData = $driver->call('', $this->customerOptions());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        if ($customerData->has('error')) {
            $this->outputError($customerData->get('error'));
            return self::FAILURE;
        }

        $output = [];
        foreach ($customerData->all() as $key => $value) {
            if (!\is_array($value)) {
                $output[] = [
                    $key, $value
                ];
            } else {
                $output[] = [
                    $key, \json_encode($value)
                ];
            }
        }

        $this->table(
            ['Key', 'Value'],
            $output
        );

        return self::SUCCESS;
    }
}
