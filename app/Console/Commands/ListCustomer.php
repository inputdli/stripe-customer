<?php

namespace App\Console\Commands;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Manager;
use App\Services\Stripe\StripeCommand;

class ListCustomer extends StripeCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List stripe customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            /** @var Driver $driver */
            $driver = $this->manager->driver('all');
            $customerData = $driver->call();

        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        if ($customerData->has('error')) {
            $this->outputError($customerData->get('error'));
            return self::FAILURE;
        }

        $output = [];
        foreach($customerData->get('data') as $row) {
            $output[] = [
                $row['id'], $row['email'], $row['name']
            ];
        }

        $this->table(
            ['ID', 'Email', 'Name'],
            $output
        );

        return self::SUCCESS;
    }
}
