<?php

namespace App\Console\Commands;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Manager;
use App\Services\Stripe\StripeCommand;
use Illuminate\Console\Command;

class UpdateCustomer extends StripeCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:update {id} {--name=*} {--email=*} {--description=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update stripe customer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Customer ID is " . $this->argument('id'));
        try {
            /** @var Driver $driver */
            $driver = $this->manager->driver('update');
            $customerData = $driver->call($this->argument('id'), $this->customerOptions());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        if ($customerData->has('error')) {
            $this->outputError($customerData->get('error'));
            return self::FAILURE;
        }

        $output = [];
        foreach ($customerData->all() as $key => $value) {
            if (!\is_array($value)) {
                $output[] = [
                    $key, $value
                ];
            } else {
                $output[] = [
                    $key, \json_encode($value)
                ];
            }
        }

        $this->table(
            ['Key', 'Value'],
            $output
        );

        return self::SUCCESS;
    }

    protected function customerOptions()
    {
        $opt = [];

        if (!empty($this->option('name'))) {
            $opt['name'] = $this->option('name')[0];
        }

        if (!empty($this->option('email'))) {
            $opt['email'] = $this->option('email')[0];
        }

        if (!empty($this->option('description'))) {
            $opt['description'] = $this->option('description')[0];
        }
        return $opt;
    }
}
