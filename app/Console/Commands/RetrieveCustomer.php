<?php

namespace App\Console\Commands;

use App\Services\Stripe\Contracts\Driver;
use App\Services\Stripe\Manager;
use App\Services\Stripe\StripeCommand;
use Illuminate\Support\Arr;

class RetrieveCustomer extends StripeCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:retrieve {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve stripe customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Customer ID is ".$this->argument('id'));
        try {
            /** @var Driver $driver */
            $driver = $this->manager->driver('retrieve');
            $customerData = $driver->call($this->argument('id'));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        if ($customerData->has('error')) {
            $this->outputError($customerData->get('error'));
            return self::FAILURE;
        }

        $output = [];
        foreach($customerData->all() as $key=>$value) {
            if (!\is_array($value)) {
                $output[] = [
                    $key, $value
                ];
            } else {
                $output[] = [
                    $key, \json_encode($value)
                ];
            }
        }

        $this->table(
            ['Key', 'Value'],
            $output
        );

        return self::SUCCESS;
    }
}
