# Laravel stripe customer client 

This is an example about using Laravel to access stripe customer resources.

## Requirements

* PHP >= `7.4`
    * OpenSSL PHP Extension
    * PHP JSON Extension
    * Mbstring PHP Extension
* Composer `latest version`


## Download
### Via `git`
`git clone https://bitbucket.org/inputdli/stripe-customer.git`

## Installation

Go to `repo` directory and run `composer install`.

## Setup

Open your `.env` file, if you don't have it, please use command `cp .env.example .env` to create one. 

Find out below 
```bash
# stripe
STRIPE_API="https://api.stripe.com/v1"
STRIPE_PK={PK}
STRIPE_SK={SK}
```

Please make sure you replace your stripe public key and secure key to `{PK}` and '{SK}'.

## Get started

You can use below command to access stripe customer resources.

### Create customer

You can create customer by using `php artisan customer:create --name=Name --email=email@address.com --description=testing`;

Try `php artisan customer:create --help` to get help.

### List customers

Use command like `php artisan customer:list`. It will list 10 customers.

### Retrieve customer

In order to retrieve single customer detail, we need customer ID like `cus_LByrqk4P1jAiud`. 

```bash
php artisan customer:retrieve cus_LByrqk4P1jAiud
```

### Update customer

In order to update customer detail, we need customer ID as well. 

```bash
php artisan customer:update cus_LByrqk4P1jAiud --name=Name --email=email@address.com --description=testing

# get more help
php artisan customer:update --help
```


## Test

### Unit test

`vendor/bin/phpunit`
